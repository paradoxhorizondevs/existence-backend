package com.paradoxical.existence.infrastructure.supabase;

public interface SupabaseProfileApi {
    SupabaseUserResponse getUserWithId(String id);
}
