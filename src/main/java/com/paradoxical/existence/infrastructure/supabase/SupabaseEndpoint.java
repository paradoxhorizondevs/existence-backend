package com.paradoxical.existence.infrastructure.supabase;

public class SupabaseEndpoint {

    public static class Authentication {

        public static final String SIGNUP = "/auth/v1/signup";
        public static final String TOKEN = "/auth/v1/token";
    }

    public static class Profile {

        public static final String INDEX = "/rest/v1/profiles";
    }
}
