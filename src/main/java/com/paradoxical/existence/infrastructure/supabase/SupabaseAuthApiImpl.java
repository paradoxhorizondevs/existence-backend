package com.paradoxical.existence.infrastructure.supabase;

import com.paradoxical.existence.common.structure.HttpStructure;
import com.paradoxical.existence.domain.UserDto;
import com.paradoxical.existence.web.auth.AuthService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Objects;

@Service
public class SupabaseAuthApiImpl implements SupabaseAuthApi {

    @Value("${supabase.key.anon.public}")
    private String supabasePublicKey;
    @Value("${supabase.server}")
    private String supabaseServer;
    private final RestTemplate restTemplate;
    private final AuthService authService;

    public SupabaseAuthApiImpl(RestTemplate restTemplate,
                               AuthService authService) {
        this.restTemplate = restTemplate;
        this.authService = authService;
    }

    @Override
    public SupabaseAuthResponse signup(UserDto userDto) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(SupabaseHttpStructure.Header.API_KEY, supabasePublicKey);

        String resourceUrl = supabaseServer + SupabaseEndpoint.Authentication.SIGNUP;

        HttpEntity<UserDto> entity = new HttpEntity<>(userDto, headers);
        ResponseEntity<SupabaseAuthResponse> response = restTemplate.exchange(
                resourceUrl,
                HttpMethod.POST,
                entity,
                SupabaseAuthResponse.class);

        SupabaseUserResponse userResponse = Objects.requireNonNull(response.getBody()).getUser();
        authService.updateSecurityContext(userResponse.getId());
        return response.getBody();
    }

    public SupabaseAuthResponse login(UserDto userDto) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(SupabaseHttpStructure.Header.API_KEY, supabasePublicKey);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        String queryValue = HttpStructure.Parameter.PASSWORD;
        params.add(HttpStructure.Parameter.GRANT_TYPE, queryValue);

        String resourceUrl = supabaseServer + SupabaseEndpoint.Authentication.TOKEN;
        URI url = UriComponentsBuilder
                .fromUriString(resourceUrl)
                .queryParams(params)
                .build()
                .toUri();

        HttpEntity<UserDto> entity = new HttpEntity<>(userDto, headers);
        ResponseEntity<SupabaseAuthResponse> response = restTemplate.exchange(
                url,
                HttpMethod.POST,
                entity,
                SupabaseAuthResponse.class
        );

        SupabaseUserResponse userResponse = Objects.requireNonNull(response.getBody()).getUser();
        authService.updateSecurityContext(userResponse.getId());
        return response.getBody();
    }


}
