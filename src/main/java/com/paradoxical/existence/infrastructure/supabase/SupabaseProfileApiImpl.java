package com.paradoxical.existence.infrastructure.supabase;

import com.paradoxical.existence.common.structure.HttpStructure;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Service
public class SupabaseProfileApiImpl implements SupabaseProfileApi {

    @Value("${supabase.key.anon.public}")
    private String supabasePublicKey;
    @Value("${supabase.server}")
    private String supabaseServer;
    private final RestTemplate restTemplate;

    public SupabaseProfileApiImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public SupabaseUserResponse getUserWithId(String id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(SupabaseHttpStructure.Header.API_KEY, supabasePublicKey);
        headers.add(HttpHeaders.ACCEPT, HttpStructure.Header.ACCEPTS_TYPE);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        String queryValue = "eq." + id;
        params.add("id", queryValue);

        String resourceUrl = supabaseServer + SupabaseEndpoint.Profile.INDEX;
        URI url = UriComponentsBuilder
                .fromUriString(resourceUrl)
                .queryParams(params)
                .build()
                .toUri();

        HttpEntity<Object> entity = new HttpEntity<>(null, headers);
        ResponseEntity<SupabaseUserResponse> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                entity,
                SupabaseUserResponse.class
        );

        return response.getBody();
    }
}
