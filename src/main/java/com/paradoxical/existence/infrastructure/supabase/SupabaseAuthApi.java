package com.paradoxical.existence.infrastructure.supabase;

import com.paradoxical.existence.domain.UserDto;

public interface SupabaseAuthApi {
    SupabaseAuthResponse signup(UserDto userDto);
    SupabaseAuthResponse login(UserDto userDto);
}
