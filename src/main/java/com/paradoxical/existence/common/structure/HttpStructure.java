package com.paradoxical.existence.common.structure;

public class HttpStructure {

    public static class Header {
        public static final String BEARER_PREFIX = "Bearer ";
        public static final String ACCEPTS_TYPE = "application/vnd.pgrst.object+json";
    }

    public static class Parameter {
        public static final String GRANT_TYPE = "grant_type";
        public static final String PASSWORD = "password";
    }
}
