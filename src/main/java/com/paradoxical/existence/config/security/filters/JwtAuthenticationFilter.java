package com.paradoxical.existence.config.security.filters;

import com.paradoxical.existence.common.structure.HttpStructure;
import com.paradoxical.existence.common.utils.JwtUtil;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtUtil jwtUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        String userId = null;
        String authToken;

        if (authHeader != null && authHeader.startsWith(HttpStructure.Header.BEARER_PREFIX)) {
            authToken = authHeader.replace(HttpStructure.Header.BEARER_PREFIX, "");
            try {
                userId = jwtUtil.getUsernameFromToken(authToken);
            } catch (IllegalArgumentException e) {
                logger.warn("An error occurred while fetching username from token", e);
                SecurityContextHolder.clearContext();
            } catch (MalformedJwtException e) {
                logger.warn("The token is invalid.", e);
                SecurityContextHolder.clearContext();
            } catch (ExpiredJwtException e) {
                logger.warn("The token has expired.", e);
                SecurityContextHolder.clearContext();
            } catch(SignatureException e){
                logger.warn("Authentication failed. Token signature not valid.");
                SecurityContextHolder.clearContext();
            }
        } else if (authHeader == null) {
            SecurityContextHolder.clearContext();
            logger.warn("Authorization header not present.");
        } else {
            SecurityContextHolder.clearContext();
            logger.warn("Couldn't find bearer string, authorization header will be ignored.");
        }

        filterChain.doFilter(request, response);
    }
}
