package com.paradoxical.existence.config.security.service.impl;

import com.paradoxical.existence.infrastructure.supabase.SupabaseProfileApi;
import com.paradoxical.existence.infrastructure.supabase.SupabaseUserResponse;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class UserServiceImpl implements UserDetailsService {

    private final SupabaseProfileApi supabaseProfileApi;

    public UserServiceImpl(SupabaseProfileApi supabaseProfileApi) {
        this.supabaseProfileApi = supabaseProfileApi;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SupabaseUserResponse userResponse = supabaseProfileApi.getUserWithId(username);

        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_AUTHENTICATED"));
        return new User(userResponse.getId(), "", authorities);
    }
}
