package com.paradoxical.existence.web.auth;

import com.paradoxical.existence.config.security.provider.UsernamePasswordAuthTokenProvider;
import com.paradoxical.existence.config.security.service.impl.UserServiceImpl;
import com.paradoxical.existence.domain.UserDto;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    private final UsernamePasswordAuthTokenProvider tokenProvider;
    private final UserServiceImpl userService;

    public AuthServiceImpl(UsernamePasswordAuthTokenProvider tokenProvider,
                               UserServiceImpl userService) {
        this.userService = userService;
        this.tokenProvider = tokenProvider;
    }

    @Override
    public UserDto getLoginCredentials(AuthRequest authRequest) {
        UserDto userDto = new UserDto();
        userDto.setEmail(authRequest.getEmail());
        userDto.setPassword(authRequest.getPassword());
        userDto.setUsername(authRequest.getUsername());
        return userDto;
    }

    @Override
    public void updateSecurityContext(String userId) {
        UserDetails userDetails = userService.loadUserByUsername(userId);
        UsernamePasswordAuthenticationToken authenticationToken = tokenProvider.createToken(userDetails);
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
    }
}
