package com.paradoxical.existence.web.auth;

import com.paradoxical.existence.domain.UserDto;

public interface AuthService {
    UserDto getLoginCredentials(AuthRequest authRequest);
    void updateSecurityContext(String userId);
}
