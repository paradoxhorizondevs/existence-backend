package com.paradoxical.existence.web.auth;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AuthRequest {

    private String email;
    private String password;
    private String username;

    // TODO: Make it so that username is only required on user signup and not on login.
    // TODO: Modify supabase database so that user signup with automatically create a user profile with an
    //  associated username.
    // TODO: Modify supabase infrastructure so that user signup fails if a username is not supplied.
    // TODO: Modify supabase database so that a username is associated with a transaction and is returned on API calls.
}
