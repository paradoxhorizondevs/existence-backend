package com.paradoxical.existence.web.auth;

import com.paradoxical.existence.domain.UserDto;
import com.paradoxical.existence.infrastructure.supabase.SupabaseAuthApi;
import com.paradoxical.existence.infrastructure.supabase.SupabaseAuthResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/auth")
@RestController
public class AuthResource {

    private final SupabaseAuthApi supabaseAuthApi;
    private final AuthService authService;

    public AuthResource(SupabaseAuthApi supabaseAuthApi, AuthService authService) {
        this.supabaseAuthApi = supabaseAuthApi;
        this.authService = authService;
    }

    @PostMapping("/public/signup")
    public ResponseEntity<SupabaseAuthResponse> signup(@RequestBody AuthRequest authRequest) {
        UserDto userDto = authService.getLoginCredentials(authRequest);
        return new ResponseEntity<>(supabaseAuthApi.signup(userDto), HttpStatus.OK);
    }

    @PostMapping("/public/login")
    public ResponseEntity<SupabaseAuthResponse> login(@RequestBody AuthRequest authRequest) {
        UserDto userDto = authService.getLoginCredentials(authRequest);
        return new ResponseEntity<>(supabaseAuthApi.login(userDto), HttpStatus.OK);
    }
}
