package com.paradoxical.existence.web.test;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/test")
@RestController
public class TestResource {

    @GetMapping("/public/hello")
    public String helloPublic() {
        return "Hello World!";
    }

    @GetMapping("/auth/hello")
    public String helloSecured() {
        return "Hello World!";
    }
}
