package com.paradoxical.existence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExistenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExistenceApplication.class, args);
	}

}
